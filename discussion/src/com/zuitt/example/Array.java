package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    //[SECTION] Java Collection
        // These are single unit of objects
        //Useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops.
    public static void main(String[] args){
        //[SECTION] Array
            //In Java, arrays are containers of values of the same type given a predefined amount of values.
            //Java arrays are more rigid; Once the size and data type are defined, they can no longer be changed.

        //Array Declaration
            //Syntax:
            //dataType[] identifier = new dataType[numOfElements];
                //"[]" indicates that the data type should be able to hold multiple values.
                //"new" keyword is for the non-primitive data types to tell Java to create the said variable.
                //The values of the array is initialized either with 0 or null.

        int[] intArray = new int[5];

        intArray[0]=200;
        intArray[1]=3;
        intArray[2]=25;
        intArray[3]=50;
        intArray[4]=99;
        //intArray[5]=100; out bounds error

        //This will return.print the memory address of the array.
        //System.out.println(intArray);

        //To print the intArray, we need to import the Array class and use the .toString() method to convert the array into string when we print it into the terminal.
        System.out.println(Arrays.toString(intArray));

        //Declaration with Initialization
            //Syntax:
            //dataType[] identifier = {elementA,elementB,elementC,....};
                //The compiler automatically specifies the size by counting the number of elements in the array.

        String[] names={"John","Jane","Joe"};
        /*names[3]="Joey";*/

        System.out.println(Arrays.toString(names));

        //Sample java array methods:
        //Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after the sort method: "+ Arrays.toString(intArray));

        Arrays.sort(names);
        System.out.println("Order of items after the sort method: "+ Arrays.toString(names));

        //Multidimensional Arrays
            //A two-dimensional array can be described by two lengths nested within each other,like a matrix
            //first length is row, second length is column

        String[][]classroom=new String[3][3];

        //First Row
        classroom[0][0]="Athos";
        classroom[0][1]="Porthos";
        classroom[0][2]="Aramis";

        //Second Row
        classroom[1][0]="Brandon";
        classroom[1][1]="JunJun";
        classroom[1][2]="Jobert";

        //Third Row
        classroom[2][0]="Mickey Mouse";
        classroom[2][1]="Donald Duck";
        classroom[2][2]="Goofy";

        //System.out.println(Arrays.toString(classroom));
        //we use the deepToString() method in accessing values for multidimensional arrays.
        System.out.println(Arrays.deepToString(classroom));

        //In Java, the size of the array cannot be modified. If there is a need to add of remove elements,new arrays must be created.

        //[SECTION] Arraylists
            //are resizable arrays, wherein elements can be added or removed whenever it is needed.

            //Syntax:
                //ArrayList<T> identifier = new ArrayList<T>();
                //"<T>" is used to specify that the list can only have one type of object in a collection.
                //Arraylist cannot hold primitive data types, "java wrapper class" provides a way to use these type of data as object.
                    //in short,Object version of primitive data types with method.

            //Declaring an ArrayList
        //ArrayList<int> numbers=new ArrayList<int>(); // Type argument cannot be of primitive type.

        ArrayList<Integer> numbers=new ArrayList<Integer>(); //valid

        //Declaring an Arraylist then initializing
        //ArrayList<String> students=new ArrayList<String>();

        //Declaring an ArrayList with values
        ArrayList<String> students=new ArrayList<String>(Arrays.asList("Jane","Mike"));

        //add elements
        //Syntax:arrayListName.add(element);
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        //Access an element/s
        //arrayListName.get(index);
        System.out.println(students.get(0));
       // System.out.println(students.get(5)); //out of bounds
        //Add an element to a specific index.
        //arrayListName.add(index, element);
        students.add(0,"Joey");
        System.out.println(students);

        //Updating an element
        //arrayListName.set(index,element);
        students.set(0,"George");
        System.out.println(students);

        //Removing a specific element
        //arrayListName.remove(index);
        students.remove(1);
        System.out.println(students);

        //Removing all elements
        students.clear();
        System.out.println(students);

        //Getting the arrayList size
        System.out.println((students.size()));

        //[SECTION] Hashmaps
            //most objects in Java are defined and are instantiations of Classes that contain a set of properties and methods.

            //Syntax:
                //HashMap<dataTypeField,dataTypeValue> identifier=new HashMap<>();
        //Declaring hashmaps
        //HashMap<String,String> jobPosition=new HashMap<String,String>();

        //Declaring Hashmaps with initialization
        HashMap<String,String> jobPosition=new HashMap<String,String>(){
            {
                put("Teacher","John");
                put("Artist","Jane");
            }
        };

        System.out.println(jobPosition);

        //Add elements
        //hashMapName.put(<fieldName>,<value>);
        jobPosition.put("Student","Brandon");
        jobPosition.put("Dreamer","Alice");
        //jobPosition.put("Student","Jane");the last one will be overridden whenever the same key is used.
        System.out.println(jobPosition);

        //Accessing element - we use the field name because they are unique.
        //hashMapName.get("fieldName");
        System.out.println(jobPosition.get("Student"));
        //System.out.println(jobPosition.get("student"));this will print null
        //System.out.println(jobPosition.get("Admin"));This will also print null

        //Updating the value
        //hashMapName.replace("fieldNameToChange","newValue");
        jobPosition.replace("Student","Brandon Smith");
        System.out.println(jobPosition);

        //Removing an element
        //hashMapName.remove(key);
        jobPosition.remove("Dreamer");
        System.out.println(jobPosition);

        //Remove all elements
        //hashMapName.clear();
        jobPosition.clear();
        System.out.println(jobPosition);
    }
}











